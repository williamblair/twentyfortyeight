#include <kernel.h>
#include <sifrpc.h>
#include <sbv_patches.h>
#include <loadfile.h>
#include <tamtypes.h>
#include <audsrv.h>
#include "../data/audsrv.h" // audsrv.irx data array
#include "../data/cdfs.h" // cdfs.irx data array

#include <tamtypes.h>
#include <kernel.h>
#include <sifrpc.h>
#include <loadfile.h>
#include <fcntl.h>
#include <unistd.h>
#include <sjis.h>
#include <malloc.h>
#include <libmc.h>
#include <stdio.h>
#include <string.h>

static inline bool PlatformInit()
{
    int irxRet;
    int ret;
    

    SifInitRpc( 0 );

    // enable load modules from EE ram
    ret = sbv_patch_enable_lmb();
    if ( ret != 0 ) {
        printf( "Load Module patch failed\n");
        return 1;
    }
    
    printf( "Ps2 Platform Init Loading LIBSD\n" );
    ret = SifLoadModule( "rom0:LIBSD", 0, NULL );
    printf( "  libsd loadmodule: %d\n", ret );

    printf( "Ps2 Platform Init Loading SIO2MAN\n" );
    ret = SifLoadModule( "rom0:SIO2MAN", 0, NULL );
    if ( ret < 0 ) {
        printf( "  Failed to load SIO2MAN\n" );
        return false;
    }
    printf( "  sio2man ret: %d\n", ret );

    printf( "Ps2 Platform Init loading MCMAN\n" );
    ret = SifLoadModule( "rom0:MCMAN", 0, NULL );
    if ( ret < 0 ) {
        printf( "Failed to load XMCMAN\n" );
        return false;
    }
    printf( "  MCMAN ret: %d\n", ret );

    printf( "Ps2 Platform Init loading MCSERV\n" );
    ret = SifLoadModule( "rom0:MCSERV", 0, NULL );
    if ( ret < 0 ) {
        printf( "Failed to load MCSERV\n" );
        return false;
    }



    printf( "Ps2 Platform Init Loading audsrv.irx buffer\n" );
    irxRet = 0;
    ret = SifExecModuleBuffer(
        audsrv_irx, // EE RAM buffer to load IRX module from
        size_audsrv_irx, // size of EE RAM buffer
        0, // length of arguments list in bytes
        nullptr, // arguments list
        &irxRet // store IRX return from _start() function
    );
    printf( "  audsrv loadmodule %d, IRX _start() ret %d\n", ret, irxRet );
    ret = audsrv_init();
    if ( ret != 0 ) {
        printf( "Failed to init audsrv\n" );
        printf( "%s\n", audsrv_get_error_string() );
        return false;
    }

    audsrv_adpcm_init();
    audsrv_set_volume( MAX_VOLUME );
    audsrv_adpcm_set_volume( 0, MAX_VOLUME );

    if ( mcInit(MC_TYPE_MC) < 0 ) {
        printf( "Failed to init memcard server\n");
        return false;
    }

    if ( SDL_Init( SDL_INIT_VIDEO|SDL_INIT_JOYSTICK|SDL_INIT_TIMER ) < 0 ) {
        printf( "Failed to init SDL\n" );
        return false;
    }

    return true;
}

