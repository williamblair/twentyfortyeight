#ifndef TWENTYFORTYEIGHT_MUSIC_H_INCLUDED
#define TWENTYFORTYEIGHT_MUSIC_H_INCLUDED

// PS2 Build
#ifdef _EE
    #include "PS2Music.h"
#endif

// PS3 build
#ifdef _PPU
    #include "PS3Music.h"
#endif

// PC Build
#ifdef _PC

#include <SDL/SDL.h>
#include <SDL/SDL_endian.h>
#include <SDL/SDL_mixer.h>

class Music
{
public:

    Music();
    ~Music();

    bool Init( const char* fileName );

    bool Play( const bool looping );

private:
    Mix_Music* mMusic;
};

#endif // end PC build

#endif

