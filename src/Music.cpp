#include <cstring>

#include "Music.h"
#include "Log.h"

#include <SDL/SDL_mixer.h>

Music::Music() :
    mMusic( nullptr )
{
}

Music::~Music()
{
    if ( mMusic != nullptr ) {
        Mix_FreeMusic( mMusic );
        mMusic = nullptr;
    }
}

bool Music::Init( const char* fileName )
{

    static char fullPathName[256];
    sprintf( fullPathName, "data/%s", fileName );
    printf( "Music loading path name: %s\n", fullPathName );

    mMusic = Mix_LoadMUS( fullPathName );
    if ( mMusic == nullptr ) {
        LOG_ERROR( "Failed to create Music" );
        return false;
    }
    

    return true;
}

bool Music::Play( const bool looping )
{
    if ( mMusic == nullptr ) {
        LOG_ERROR( "mMusic is null" );
        return false;
    }
    
    int numTimes = ( looping ? -1 : 0 );
    if ( Mix_PlayMusic( mMusic, numTimes ) < 0 ) {
        LOG_ERROR( "Mix_PlayMusic failed" );
        return false;
    }
    
    return true;
}


