#ifndef BOARD_H_INCLUDED
#define BOARD_H_INCLUDED

#include "Renderer.h"
#include "Font.h"

#define BOARD_ROWS 4
#define BOARD_COLS 4
#define BOARD_X_OFFSET 50 
#define BOARD_Y_OFFSET 50 
#define BOARD_TILE_WIDTH ((SCREEN_WIDTH-(BOARD_X_OFFSET*2))/BOARD_COLS)
#define BOARD_TILE_HEIGHT ((SCREEN_HEIGHT-(BOARD_Y_OFFSET))/BOARD_ROWS)
#define TILE_OFFSET 5 // pixels space between tiles
#define MAX_TRANSITIONS (BOARD_ROWS*BOARD_COLS)

class Board
{
public:

    Board();
    ~Board();
    
    void Draw( Renderer& render );

    void SetFont( Font& font ) {
        mFont = &font;
    }

    struct SlideTransition
    {
        int row;
        int col;
        int destRow;
        int destCol;
        int val; // the number of this tile (e.g. 2,4,8,16,...)
        float curTime; // transition time counter
        float endTime; // how long the transition is
    };
    
    // assumes result is of length MAX_TRANSITIONS
    // returns number of transition pieces
    int GetSlideUpTransitions(SlideTransition* result);
    int GetSlideDownTransitions(SlideTransition* result);
    int GetSlideLeftTransitions(SlideTransition* result);
    int GetSlideRightTransitions(SlideTransition* result);
    
    // Copy the internal temp board generated during GetSlide* into the actual board
    void ApplyLastTransitions();

    void DrawWithTransitions( Renderer& render, SlideTransition* transitions, const int numTransitions );

    struct Space
    {
        int row;
        int col;
    };
    // assumes spaces is of length (BOARD_ROWS*BOARD_COLS)
    // returns the number of entries put in spaces
    int GetFreeSpaces(Space* spaces);
    
    void SetSpace( const int row, const int col, const int val ) {
        mBoard[row][col] = val;
    }

    unsigned int GetScore() const {
        return mScore;
    }
    void Reset();

private:

    int mBoard[BOARD_ROWS][BOARD_COLS];
    
    // for creating transition info
    int mTmpBoard[BOARD_ROWS][BOARD_COLS];

    unsigned int mScore;
    unsigned int mTmpScore; // keep track of score during transitions
    
    Font* mFont;
};



#endif // BOARD_H_INCLUDED
