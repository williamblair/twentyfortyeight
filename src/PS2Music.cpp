#include "PS2Music.h"
#include "Log.h"
#include "../data/stage1.h"

#include <cstdio>
#include <cstring>

#include <kernel.h>
#include <sifrpc.h>
#include <loadfile.h>
#include <tamtypes.h>


bool Music::musicLibInitted = false;

Music::Music()
{
}

Music::~Music()
{
}

struct MusicData
{
    const char* fileName;
    unsigned char* fileData;
    unsigned int fileDataLength;
};
static MusicData musicDataArr[1] = {
    {
        "stage1.wav",
        stage1_adp, // stage1.h
        size_stage1_adp
    }
};
static const int musicDataArrSize = 1;

bool Music::Init( const char* fileName )
{
    if ( !musicLibInitted ) {
        /*int ret = audsrv_init();
        if ( ret != 0 ) {
            LOG_ERROR( "Failed to init audsrv" );
            LOG_ERROR( audsrv_get_error_string() );
            return false;
        }

        audsrv_adpcm_init();
        audsrv_set_volume( MAX_VOLUME );
        audsrv_adpcm_set_volume( 0, MAX_VOLUME );*/
        musicLibInitted = true;
    }

    MusicData* md = nullptr;
    for ( int i = 0; i < musicDataArrSize && md == nullptr; ++i ) {
        if ( strcmp( musicDataArr[i].fileName, fileName ) == 0 ) {
            md = &musicDataArr[i];
        }
    }
    if ( md == nullptr ) {
        LOG_ERROR( "Failed to find matching array of filename" );
        return false;
    }

    audsrv_load_adpcm( &mSample, md->fileData, md->fileDataLength );

    return true;
}

bool Music::Play( const bool looping )
{
    // Looping is built into the file...
    audsrv_ch_play_adpcm( 0, &mSample );

    return true;
}

