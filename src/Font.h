#ifndef FONT_H_INCLUDED
#define FONT_H_INCLUDED

#include "Image.h"
#include "Renderer.h"

class Font
{
public:

    Font();
    ~Font();

    /*
        @brief Load character image atlas and settings
        @param [imageFile] full path of the atlas image file
        @param [charWidth] width in px of a single character
        @param [charHeight] height in px of a single character
        @return true if the image was successfully loaded, false otherwise
    */
    bool Init(
        const char* imageFile,
        const int charWidth,
        const int charHeight
    );

    /*
        @brief draw text using this font
        @param [render] the renderer to draw with
        @param [x] the top left x to start drawing from
        @param [y] the top left y to start drawing from
        @param [msg] the letters to draw
    */
    void Draw(
        Renderer& render,
        int x,
        int y,
        const char* msg
    );

    int getCharWidth() { return mCharWidth; }
    int getCharHeight() { return mCharHeight; }

private:

    int mCharWidth;
    int mCharHeight;

    int mCharsPerRow;

    Image mImage;
};

#endif // FONT_H_INCLUDED

