#include <cstring>

#include "Music.h"
#include "Log.h"

#include <SDL/SDL_mixer.h>

Music::Music() :
    mMusic( nullptr )
{
}

Music::~Music()
{
    if ( mMusic != nullptr ) {
        Mix_FreeChunk( mMusic );
        mMusic = nullptr;
    }
}

bool Music::Init( const char* fileName )
{

    static char fullPathName[256];
    
    sprintf( fullPathName, "%s/%s", ASSETS_DIR, fileName );
    printf( "Music loading path name: %s\n", fullPathName );

    mMusic = Mix_LoadWAV( fullPathName );
    if ( mMusic == nullptr ) {
        LOG_ERROR( "Failed to create Music" );
        return false;
    }
    

    return true;
}

bool Music::Play( const bool looping )
{
    if ( mMusic == nullptr ) {
        LOG_ERROR( "mMusic is null" );
        return false;
    }
    
    if ( Mix_PlayChannel( 0, mMusic, looping ? -1 : 0 ) == -1 ) {
        LOG_ERROR( "Mix_PlayChannel failed" );
        return false;
    }
    
    return true;
}


