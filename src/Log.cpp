#include "Log.h"

#include <stdio.h>

namespace Log
{

void Error(const char* file, const int line, const char* message)
{
    printf( "%s:%d: Error: %s\n", file, line, message );
}

} // end namespace Log
