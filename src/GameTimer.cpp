#include "GameTimer.h"

GameTimer::GameTimer() :
    mLastTicks( 0 ),
    mFPS( 0.0 ),
    mDT( 0.0 )
{
}

GameTimer::~GameTimer()
{
}

void GameTimer::Update()
{
    Uint32 thisTicks = SDL_GetTicks();
    Uint32 ticksPassed = thisTicks - mLastTicks;
    
    mDT = ((double)ticksPassed) / 1000.0;
    
    mFPS = 1.0 / mDT;
    
    mLastTicks = thisTicks;
}
