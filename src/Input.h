#ifndef INPUT_H_INCLUDED
#define INPUT_H_INCLUDED

#include <SDL/SDL.h>

class Input
{
public:

    enum Move
    {
        UP,
        DOWN,
        LEFT,
        RIGHT,
        NONE
    };

    Input();
    ~Input();

    bool Init();
    
    void Update();

    void Enable( bool enable ) {
        mEnabled = enable;
    }
    
    inline bool Confirm() {
        bool res = mConfirm;
        mConfirm = false;
        return res;
    }
    inline bool Quit() {
        return mQuit;
    }
    Move GetMove()
    {
        Move ret = NONE;
        if ( mMoveUp ) {
            ret = UP;
            mMoveUp = false;
        } else if ( mMoveDown ) {
            ret = DOWN;
            mMoveDown = false;
        }
        else if ( mMoveLeft ) {
            ret = LEFT;
            mMoveLeft = false;
        }
        else if ( mMoveRight ) {
            ret = RIGHT;
            mMoveRight = false;
        }
        return ret;
    }

    void Reset();

private:
    bool mEnabled;
    bool mConfirm;
    bool mQuit;
    bool mMoveLeft;
    bool mMoveRight;
    bool mMoveDown;
    bool mMoveUp;

    SDL_Joystick* mJoy;
};

#endif // INPUT_H_INCLUDED

