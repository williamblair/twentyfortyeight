#ifndef GAME_TIMER_H_INCLUDED
#define GAME_TIMER_H_INCLUDED

#include <SDL/SDL.h>

class GameTimer
{
public:

    GameTimer();
    ~GameTimer();
    
    void Update();
    
    double GetFPS() const { return mFPS; }
    double GetDeltaTime() const { return mDT; }

private:
    Uint32 mLastTicks;
    double mFPS;
    double mDT;
};

#endif // GAME_TIMER_H_INCLUDED
