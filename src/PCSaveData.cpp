#include <cstdio>
#include "Log.h"
#include "SaveData.h"


static const char* dataFileName = "data/savedata.bin";

bool SaveData::Save()
{
    FILE* fp = fopen( dataFileName, "wb" );
    if ( !fp ) {
        LOG_ERROR( "Failed to open save file" );
        return false;
    }
    int amnt = fwrite( &highScore, 1, sizeof( highScore), fp );
    bool res = true;
    if ( amnt != sizeof( highScore ) ) {
        LOG_ERROR( "Failed to write expected size to save file" );
        res = false;
    }
    fclose( fp );
    fp = nullptr;
    return res;
}

bool SaveData::Load()
{
    FILE* fp = fopen( dataFileName, "rb" );
    if ( !fp ) {
        LOG_ERROR( "Failed to open save file" );
        return false;
    }
    int amnt = fread( &highScore, 1, sizeof( highScore ), fp );
    bool res = true;
    if ( amnt != sizeof( highScore ) ) {
        LOG_ERROR( "Failed to read expected size from save file" );
        res = false;
    }
    fclose( fp );
    fp = nullptr;
    return res;
}

