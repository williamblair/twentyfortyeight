#include <audsrv.h>

class Music
{
public:

    Music();
    ~Music();

    bool Init( const char* fileName );

    bool Play( const bool looping );

private:
    static bool musicLibInitted;

    audsrv_adpcm_t mSample;
};

