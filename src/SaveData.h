#ifndef SAVE_DATA_H_INCLUDED
#define SAVE_DATA_H_INCLUDED

struct SaveData
{
    unsigned int highScore; // highest score value achieved

    // saves the data from this struct to the system
    bool Save();

    // loads the data from the system into this struct
    bool Load();
};


#endif

