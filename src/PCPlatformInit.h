static inline bool PlatformInit()
{
    if ( SDL_Init( SDL_INIT_VIDEO|SDL_INIT_JOYSTICK|SDL_INIT_TIMER|SDL_INIT_AUDIO ) < 0 ) {
        printf( "Failed to init SDL\n" );
        return false;
    }

    int mixFlags = MIX_INIT_OGG;
    if ( (Mix_Init( mixFlags ) & mixFlags) != mixFlags ) {
        printf( "Failed to init SDL mixer: %s\n", Mix_GetError() );
        return false;
    }
    if ( Mix_OpenAudio( MIX_DEFAULT_FREQUENCY,
            MIX_DEFAULT_FORMAT,
            MIX_DEFAULT_CHANNELS,
            1024 ) < 0 ) {
        printf( "Failed to init audio: %s\n", Mix_GetError() );
        return false;
    }

    return true;
}

