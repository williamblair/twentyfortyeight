#include "Input.h"
#include "Log.h"
#include <SDL/SDL.h>

// PS3 build
#ifdef _PPU
#define BUTTON_CONFIRM 9 // cross
#define BUTTON_UP 3
#define BUTTON_DOWN 1
#define BUTTON_LEFT 0
#define BUTTON_RIGHT 2
#endif
// PS2 build
#ifdef _EE
#define BUTTON_CONFIRM 1
#define BUTTON_UP -1
#define BUTTON_DOWN -1
#define BUTTON_LEFT -1
#define BUTTON_RIGHT -1
#endif
// PC build
#ifdef _PC
#define BUTTON_CONFIRM 1
#define BUTTON_UP -1
#define BUTTON_DOWN -1
#define BUTTON_LEFT -1
#define BUTTON_RIGHT -1
#endif

Input::Input() :
    mEnabled( true ),
    mConfirm( false ),
    mQuit( false ),
    mMoveLeft( false ),
    mMoveRight( false ),
    mMoveDown( false ),
    mMoveUp( false ),
    mJoy( nullptr )
{
}

Input::~Input()
{
    if ( mJoy ) {
        SDL_JoystickClose( mJoy );
        mJoy = nullptr;
    }
}

bool Input::Init()
{
    printf( "Input Init\n" );
    if ( SDL_NumJoysticks() > 0 )
    {
        mJoy = SDL_JoystickOpen( 0 );
        if ( !mJoy ) {
            LOG_ERROR( "Failed to open joystick" );
            return false;
        }
        printf("Openend joystick 0\n");
        printf("Name: %s\n", SDL_JoystickName(0));
        printf("Number of Axes: %d\n", SDL_JoystickNumAxes(mJoy));
        printf("Number of Buttons: %d\n", SDL_JoystickNumButtons(mJoy));
        printf("Number of Balls: %d\n", SDL_JoystickNumBalls(mJoy));
    }
    else
    {
        printf( "No joysticks found\n" );
    }

    return true;
}

void Input::Update()
{
    if ( !mEnabled ) {
        mConfirm = false;
        mQuit = false;
        mMoveLeft = false;        
        mMoveRight = false;        
        mMoveUp = false;        
        mMoveDown = false;        
        return;
    }
    
    SDL_Event e;
    while ( SDL_PollEvent( &e ) )
    {
        switch ( e.type )
        {
        case SDL_QUIT:
            mQuit = true;
            break;
        case SDL_KEYUP:
            if ( e.key.keysym.sym == SDLK_SPACE ) {
                mConfirm = true;
            }
            else if ( e.key.keysym.sym == SDLK_LEFT ) {
                mMoveLeft = true;
            }
            else if ( e.key.keysym.sym == SDLK_RIGHT ) {
                mMoveRight = true;
            }
            else if ( e.key.keysym.sym == SDLK_DOWN ) {
                mMoveDown = true;
            }
            else if ( e.key.keysym.sym == SDLK_UP ) {
                mMoveUp = true;
            }
            break;
        /*case SDL_JOYAXISMOTION:
            printf("Joystick %d axis %d value: %d\n",
                       e.jaxis.which,
                       e.jaxis.axis,
                       e.jaxis.value);
            break;*/
        case SDL_JOYHATMOTION:
            printf("Joystick %d hat %d value:",
                   e.jhat.which,
                   e.jhat.hat);
            if ( e.jhat.value == SDL_HAT_CENTERED ) {
                printf(" centered");
            }
            if ( e.jhat.value & SDL_HAT_UP ) {
                printf(" up");
                mMoveUp = true;
            }
            if ( e.jhat.value & SDL_HAT_RIGHT ) {
                printf(" right");
                mMoveRight = true;
            }
            if ( e.jhat.value & SDL_HAT_DOWN ) {
                printf(" down");
                mMoveDown = true;
            }
            if ( e.jhat.value & SDL_HAT_LEFT ) {
                printf(" left");
                mMoveLeft = true;
            }
            printf("\n");
            break;
        /*case SDL_JOYBALLMOTION:
            printf("Joystick %d ball %d delta: (%d,%d)\n",
                   e.jball.which,
                   e.jball.ball,
                   e.jball.xrel,
                   e.jball.yrel);
            break;*/
        case SDL_JOYBUTTONDOWN:
            printf("Joystick %d button %d down\n",
                   e.jbutton.which,
                   e.jbutton.button);
            break;
        case SDL_JOYBUTTONUP:
            printf("Joystick %d button %d up\n",
                   e.jbutton.which,
                   e.jbutton.button);
            if ( e.jbutton.button == BUTTON_CONFIRM ) {
                mConfirm = true;
            }
            else if ( e.jbutton.button == BUTTON_UP ) {
                mMoveUp = true;
            }
            else if ( e.jbutton.button == BUTTON_DOWN ) {
                mMoveDown = true;
            }
            else if ( e.jbutton.button == BUTTON_LEFT ) {
                mMoveLeft = true;
            }
            else if ( e.jbutton.button == BUTTON_RIGHT ) {
                mMoveRight = true;
            }
            break;
        default:
            break;
        }
    }
}

void Input::Reset()
{
    mEnabled = true;
    mConfirm = false;
    mQuit = false;
    mMoveLeft = false;
    mMoveRight = false;
    mMoveDown = false;
    mMoveUp = false;
    mJoy = nullptr;
}

