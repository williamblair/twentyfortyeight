#include <cstring>

#include "Font.h"
#include "Log.h"


Font::Font() :
    mCharWidth( 0 ),
    mCharHeight( 0 ),
    mCharsPerRow( 0 )
{
}

Font::~Font()
{
}

bool Font::Init(
    const char* imageFile,
    const int charWidth,
    const int charHeight
)
{
    mCharWidth = charWidth;
    mCharHeight = charHeight;

    if ( !mImage.Init( imageFile ) ) {
        LOG_ERROR( "Font failed to init mImage\n" );
        return false;
    }

    mCharsPerRow = mImage.GetWidth() / mCharWidth;

    return true;
}

void Font::Draw( Renderer& render, int x, int y, const char* msg )
{
    const int msgLen = strlen( msg );
    for (int i = 0; i < msgLen; ++i) {
        char letter = msg[i];
        // convert to uppercase if necessary
        if ( letter >= 'a' && letter <= 'z' ) {
            letter += 'A' - 'a';
        }

        // TODO - assumes starts at space char; handle otherwise
        int glyphIndex = letter - ' ';
        int v = glyphIndex / mCharsPerRow;
        int u = glyphIndex % mCharsPerRow;
        v *= mCharHeight;
        u *= mCharWidth;
        if ( u < 0 || u + mCharWidth > mImage.GetWidth() ||
             v < 0 || v + mCharWidth > mImage.GetHeight() ) {
            LOG_ERROR( "Invalid uv calc\n" );
            return;
        } 
        render.DrawImage( mImage, x, y, u, v, mCharWidth, mCharHeight );

        x += mCharWidth;
    }
}


