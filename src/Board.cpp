#include "Board.h"
#include "Log.h"
#include <cstdlib>
#include <ctime>
#include <cassert>
#include <cstring>

#define DBG 1
#if DBG
#define DBG_ASSERT(x) assert(x)
#else
#define DBG_ASSERT(x)
#endif

Board::Board() :
    mScore( 0 ),
    mTmpScore( 0 ),
	mFont( nullptr )
{
    Reset();
}

Board::~Board()
{
}

void Board::Draw( Renderer& render )
{
    // clear board area
    render.DrawRect(
        BOARD_X_OFFSET, // x
        BOARD_Y_OFFSET, // y
        BOARD_COLS * BOARD_TILE_WIDTH, // w
        BOARD_ROWS * BOARD_TILE_HEIGHT, // h
        193, // r
        185, // g
        164 // b
    );

    for ( int row = 0; row < BOARD_ROWS; ++row ) {
        for ( int col = 0; col < BOARD_COLS; ++col ) {
            // draw non-empty space
            if ( mBoard[row][col] != 0 ) {
                render.DrawRect(
                    BOARD_X_OFFSET + (col * BOARD_TILE_WIDTH) + TILE_OFFSET, // x
                    BOARD_Y_OFFSET + (row * BOARD_TILE_HEIGHT) + TILE_OFFSET, // y
                    BOARD_TILE_WIDTH - TILE_OFFSET, // w
                    BOARD_TILE_HEIGHT - TILE_OFFSET, // h
                    25, // r
                    25, // g
                    25  // b
                );
				DBG_ASSERT( mFont != nullptr );
				static char numStr[32];
				sprintf( numStr, "%d", mBoard[row][col] );
                int numChars = strlen( numStr );
                int tileX = BOARD_X_OFFSET + col * BOARD_TILE_WIDTH;
                int tileY = BOARD_Y_OFFSET + row * BOARD_TILE_HEIGHT;
                int msgX = tileX + (BOARD_TILE_WIDTH/2) - (numChars/2)*mFont->getCharWidth();
                int msgY = tileY + (BOARD_TILE_HEIGHT/2) - (mFont->getCharHeight()/2);
                mFont->Draw(
                    render,
                    msgX,
                    msgY,
                    numStr
                );
            }
        }
    }
}

void Board::DrawWithTransitions( Renderer& render, 
    Board::SlideTransition* transitions, 
    const int numTransitions )
{
    // clear board area
    render.DrawRect(
        BOARD_X_OFFSET, // x
        BOARD_Y_OFFSET, // y
        BOARD_COLS * BOARD_TILE_WIDTH, // w
        BOARD_ROWS * BOARD_TILE_HEIGHT, // h
        193, // r
        185, // g
        164 // b
    );

    for ( int row = 0; row < BOARD_ROWS; ++row ) {
        for ( int col = 0; col < BOARD_COLS; ++col ) {

            // check if it's a transition tile; if so don't draw
            bool isTransition = false;
            for (int tr = 0; tr < numTransitions && !isTransition; ++tr) {
                if ( transitions[tr].row == row && transitions[tr].col == col ) {
                    isTransition = true;
                }
            }
            if ( isTransition ) {
                continue;
            }

            // draw non-empty space
            if ( mBoard[row][col] != 0 ) {
                render.DrawRect(
                    BOARD_X_OFFSET + (col * BOARD_TILE_WIDTH) + TILE_OFFSET, // x
                    BOARD_Y_OFFSET + (row * BOARD_TILE_HEIGHT) + TILE_OFFSET, // y
                    BOARD_TILE_WIDTH - TILE_OFFSET, // w
                    BOARD_TILE_HEIGHT - TILE_OFFSET, // h
                    25, // r
                    25, // g
                    25  // b
                );
				DBG_ASSERT( mFont != nullptr );
				static char numStr[32];
				sprintf( numStr, "%d", mBoard[row][col] );
                int numChars = strlen( numStr );
                int tileX = BOARD_X_OFFSET + col * BOARD_TILE_WIDTH;
                int tileY = BOARD_Y_OFFSET + row * BOARD_TILE_HEIGHT;
                int msgX = tileX + (BOARD_TILE_WIDTH/2) - (numChars/2)*mFont->getCharWidth();
                int msgY = tileY + (BOARD_TILE_HEIGHT)/2 - (mFont->getCharHeight()/2);
                mFont->Draw(
                    render,
                    msgX,
                    msgY,
                    numStr
                );
            }
        }
    }

    // draw the transitions
    for ( int tr = 0; tr < numTransitions; ++tr ) {
        SlideTransition& tran = transitions[tr];

        // interval time from 0..1
        float t = tran.curTime / tran.endTime;
        // total distance in units of rows/cols needed to travel
        float totalRowDist = tran.destRow - tran.row;
        float totalColDist = tran.destCol - tran.col;
        // offset to draw the tile (int row/col units)
        float rowOffset = totalRowDist * t;
        float colOffset = totalColDist * t;

        int tileX = BOARD_X_OFFSET + (int)((tran.col + colOffset) * (float)BOARD_TILE_WIDTH);
        int tileY = BOARD_Y_OFFSET + (int)((tran.row + rowOffset) * (float)BOARD_TILE_HEIGHT);
        render.DrawRect(
            tileX + TILE_OFFSET, // x
            tileY + TILE_OFFSET, // y
            BOARD_TILE_WIDTH - TILE_OFFSET, // w
            BOARD_TILE_HEIGHT - TILE_OFFSET, // h
            25, // r
            25, // g
            25 // b
        );
        DBG_ASSERT( mFont != nullptr );
        static char tranNumStr[32];
        sprintf( tranNumStr, "%d", tran.val );
        int numChars = strlen( tranNumStr );
        int msgX = tileX + (BOARD_TILE_WIDTH/2) - (numChars/2)*mFont->getCharWidth();
        int msgY = tileY + (BOARD_TILE_HEIGHT)/2 - (mFont->getCharHeight()/2);
        mFont->Draw(
            render,
            msgX,
            msgY,
            tranNumStr
        );
    }
}

int Board::GetSlideUpTransitions(Board::SlideTransition* result)
{
    // copy current board into temp board for modification
    for ( int row = 0; row < BOARD_ROWS; ++row )
    {
        for ( int col = 0; col < BOARD_COLS; ++col )
        {
            mTmpBoard[row][col] = mBoard[row][col];
        }
    }
    

    int numTilesMoved = 0;
    for ( int row = 1; row < BOARD_ROWS; ++row ) {
        for ( int col = 0; col < BOARD_COLS; ++col ) {
            int curTileVal = mTmpBoard[row][col];
            
            // skip empty tiles
            if ( curTileVal == 0 ) {
                continue;
            }
            
            int slideToRow = row - 1;
            while ( slideToRow > 0 && mTmpBoard[slideToRow][col] == 0 ) {
                --slideToRow;
            }
            
            // move the tile without any intersection
            if ( mTmpBoard[slideToRow][col] == 0 ) {
                mTmpBoard[slideToRow][col] = curTileVal;
                mTmpBoard[row][col] = 0;
                result->row = row;
                result->col = col;
                result->destRow = slideToRow;
                result->destCol = col;
                result->val = curTileVal;
                ++result;
                ++numTilesMoved;

                continue;
            }
            
            // tile intersects, check if the tiles can be merged...
            if ( mTmpBoard[slideToRow][col] == mTmpBoard[row][col] ) {
                mTmpBoard[slideToRow][col] += mTmpBoard[row][col];
                mTmpScore += mTmpBoard[slideToRow][col];
                mTmpBoard[row][col] = 0;

                result->row = row;
                result->col = col;
                result->destRow = slideToRow;
                result->destCol = col;
                result->val = curTileVal;
                ++result;
                ++numTilesMoved;

            }
            // tile intersects but cannot be merged, target is moved down 1
            else {
                int resultRow = slideToRow + 1;
                if (resultRow != row) {
                    mTmpBoard[resultRow][col] = mTmpBoard[row][col];
                    mTmpBoard[row][col] = 0;
                    result->row = row;
                    result->col = col;
                    result->destRow = resultRow;
                    result->destCol = col;
                    result->val = curTileVal;
                    ++result;
                    ++numTilesMoved;
                }
            }
        }
    }
    return numTilesMoved;
}

int Board::GetSlideDownTransitions(Board::SlideTransition* result)
{
    // copy current board into temp board for modification
    for ( int row = 0; row < BOARD_ROWS; ++row )
    {
        for ( int col = 0; col < BOARD_COLS; ++col )
        {
            mTmpBoard[row][col] = mBoard[row][col];
        }
    }
    

    int numTilesMoved = 0;
    for ( int row = BOARD_ROWS - 2; row >= 0; --row ) {
        for ( int col = 0; col < BOARD_COLS; ++col ) {
            int curTileVal = mTmpBoard[row][col];
            
            // skip empty tiles
            if ( curTileVal == 0 ) {
                continue;
            }
            
            int slideToRow = row + 1;
            while ( slideToRow < BOARD_ROWS-1 && mTmpBoard[slideToRow][col] == 0 ) {
                ++slideToRow;
            }
            
            // move the tile without any intersection
            if ( mTmpBoard[slideToRow][col] == 0 ) {
                mTmpBoard[slideToRow][col] = curTileVal;
                mTmpBoard[row][col] = 0;
                result->row = row;
                result->col = col;
                result->destRow = slideToRow;
                result->destCol = col;
                result->val = curTileVal;
                ++result;
                ++numTilesMoved;

                continue;
            }
            
            // tile intersects, check if the tiles can be merged...
            if ( mTmpBoard[slideToRow][col] == mTmpBoard[row][col] ) {
                mTmpBoard[slideToRow][col] += mTmpBoard[row][col];
                mTmpScore += mTmpBoard[slideToRow][col];
                mTmpBoard[row][col] = 0;

                result->row = row;
                result->col = col;
                result->destRow = slideToRow;
                result->destCol = col;
                result->val = curTileVal;
                ++result;
                ++numTilesMoved;

            }
            // tile intersects but cannot be merged, target is moved up 1
            else {
                int resultRow = slideToRow - 1;
                if (resultRow != row) {
                    mTmpBoard[resultRow][col] = mTmpBoard[row][col];
                    mTmpBoard[row][col] = 0;
                    result->row = row;
                    result->col = col;
                    result->destRow = resultRow;
                    result->destCol = col;
                    result->val = curTileVal;
                    ++result;
                    ++numTilesMoved;
                }
            }
        }
    }
    return numTilesMoved;
}

int Board::GetSlideLeftTransitions(Board::SlideTransition* result)
{
    // copy current board into temp board for modification
    for ( int row = 0; row < BOARD_ROWS; ++row )
    {
        for ( int col = 0; col < BOARD_COLS; ++col )
        {
            mTmpBoard[row][col] = mBoard[row][col];
        }
    }
    

    int numTilesMoved = 0;
    for ( int col = 1; col < BOARD_COLS; ++col ) {
        for ( int row = 0; row < BOARD_ROWS; ++row ) {
            int curTileVal = mTmpBoard[row][col];
            
            // skip empty tiles
            if ( curTileVal == 0 ) {
                continue;
            }
            
            int slideToCol = col - 1;
            while ( slideToCol > 0 && mTmpBoard[row][slideToCol] == 0 ) {
                --slideToCol;
            }
            
            // move the tile without any intersection
            if ( mTmpBoard[row][slideToCol] == 0 ) {
                mTmpBoard[row][slideToCol] = curTileVal;
                mTmpBoard[row][col] = 0;
                result->row = row;
                result->col = col;
                result->destRow = row;
                result->destCol = slideToCol;
                result->val = curTileVal;
                ++result;
                ++numTilesMoved;

                continue;
            }
            
            // tile intersects, check if the tiles can be merged...
            if ( mTmpBoard[row][slideToCol] == mTmpBoard[row][col] ) {
                mTmpBoard[row][slideToCol] += mTmpBoard[row][col];
                mTmpScore += mTmpBoard[row][slideToCol];
                mTmpBoard[row][col] = 0;

                result->row = row;
                result->col = col;
                result->destRow = row;
                result->destCol = slideToCol;
                result->val = curTileVal;
                ++result;
                ++numTilesMoved;

            }
            // tile intersects but cannot be merged, target is moved up 1
            else {
                int resultCol = slideToCol + 1;
                if (resultCol != col) {
                    mTmpBoard[row][resultCol] = mTmpBoard[row][col];
                    mTmpBoard[row][col] = 0;
                    result->row = row;
                    result->col = col;
                    result->destRow = row;
                    result->destCol = resultCol;
                    result->val = curTileVal;
                    ++result;
                    ++numTilesMoved;
                }
            }
        }
    }
    return numTilesMoved;
}

int Board::GetSlideRightTransitions(Board::SlideTransition* result)
{
    // copy current board into temp board for modification
    for ( int row = 0; row < BOARD_ROWS; ++row )
    {
        for ( int col = 0; col < BOARD_COLS; ++col )
        {
            mTmpBoard[row][col] = mBoard[row][col];
        }
    }
    

    int numTilesMoved = 0;
    for ( int col = BOARD_COLS - 2; col >= 0; --col ) {
        for ( int row = 0; row < BOARD_ROWS; ++row ) {
            int curTileVal = mTmpBoard[row][col];
            
            // skip empty tiles
            if ( curTileVal == 0 ) {
                continue;
            }
            
            int slideToCol = col + 1;
            while ( slideToCol < BOARD_COLS-1 && mTmpBoard[row][slideToCol] == 0 ) {
                ++slideToCol;
            }
            
            // move the tile without any intersection
            if ( mTmpBoard[row][slideToCol] == 0 ) {
                mTmpBoard[row][slideToCol] = curTileVal;
                mTmpBoard[row][col] = 0;
                result->row = row;
                result->col = col;
                result->destRow = row;
                result->destCol = slideToCol;
                result->val = curTileVal;
                ++result;
                ++numTilesMoved;

                continue;
            }
            
            // tile intersects, check if the tiles can be merged...
            if ( mTmpBoard[row][slideToCol] == mTmpBoard[row][col] ) {
                mTmpBoard[row][slideToCol] += mTmpBoard[row][col];
                mTmpScore += mTmpBoard[row][slideToCol];
                mTmpBoard[row][col] = 0;

                result->row = row;
                result->col = col;
                result->destRow = row;
                result->destCol = slideToCol;
                result->val = curTileVal;
                ++result;
                ++numTilesMoved;

            }
            // tile intersects but cannot be merged, target is moved up 1
            else {
                int resultCol = slideToCol - 1;
                if (resultCol != col) {
                    mTmpBoard[row][resultCol] = mTmpBoard[row][col];
                    mTmpBoard[row][col] = 0;
                    result->row = row;
                    result->col = col;
                    result->destRow = row;
                    result->destCol = resultCol;
                    result->val = curTileVal;
                    ++result;
                    ++numTilesMoved;
                }
            }
        }
    }
    return numTilesMoved;
}

void Board::ApplyLastTransitions()
{
    for ( int row = 0; row < BOARD_ROWS; ++row )
    {
        for ( int col = 0; col < BOARD_COLS; ++col )
        {
            mBoard[row][col] = mTmpBoard[row][col];
        }
    }
    mScore = mTmpScore;
}

int Board::GetFreeSpaces( Board::Space* spaces )
{
    int numFree = 0;
    for ( int row = 0; row < BOARD_ROWS; ++row )
    {
        for ( int col = 0; col < BOARD_COLS; ++col )
        {
            if ( mBoard[row][col] == 0 ) {
                spaces->row = row;
                spaces->col = col;
                ++spaces;
                ++numFree;
            }
        }
    }
    return numFree;
}

void Board::Reset()
{
    mScore = 0;
    mTmpScore = 0;
	int randRow = rand() % BOARD_ROWS;
	int randCol = rand() % BOARD_COLS;
    for ( int i = 0; i < BOARD_ROWS; ++i ) {
        for ( int j = 0; j < BOARD_COLS; ++j ) {
			if ( i == randRow && j == randCol ) {
				mBoard[i][j] = 2;
                mTmpBoard[i][j] = 2;
			}
			else {
				mBoard[i][j] = 0;
                mTmpBoard[i][j] = 0;
			}
        }
    }
}

