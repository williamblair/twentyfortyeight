#ifndef IMAGE_H_INCLUDED
#define IMAGE_H_INCLUDED

#include <SDL/SDL_image.h>

class Image
{

friend class Renderer;

public:

    Image();
    ~Image();

    /*
        @brief Load the image data from the input file name
        @param [imageFile] the full path of the image file name
        @return true if image successfully loaded, false otherwise
    */
    bool Init( const char* imageFile );

    int GetWidth() const { return mWidth; }
    int GetHeight() const { return mHeight; }

private:

    static bool imgLibInitted;

    int mWidth;
    int mHeight;

    SDL_Surface* mSurface;
};

#endif // IMAGE_H_INCLUDED

