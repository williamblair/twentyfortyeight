//#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include "Renderer.h"
#include "Input.h"
#include "GameTimer.h"
#include "Font.h"
#include "Board.h"
#include "Music.h"
#include "PlatformInit.h"
#include "SaveData.h"
#include "Log.h"


static Renderer   gRender;
static Input      gInput;
static GameTimer  gTimer;
static Font       gFont;
static Font       gFontBlack;
static Board      gBoard;
static Music      gMusic;
static SaveData   gSaveData;

#define EXIT_ON_FAIL( result ) \
    if ( !(result) ) {         \
        return 1;              \
    }

#ifdef WIN32
    #undef main
#endif

static void GameplayLoop()
{
    static Board::Space freeSpacesBuf[BOARD_ROWS*BOARD_COLS];
    static Board::SlideTransition transitionBuf[MAX_TRANSITIONS];
    char scoreStr[256];
    char highScoreStr[256];
    unsigned int lastScore = 0;
    sprintf( scoreStr, "SCORE: %u", lastScore );
    sprintf( highScoreStr, "HIGH SCORE: %u", gSaveData.highScore );

    printf("Gamepley loop\n");
    bool running = true;
    while ( running )
    {
        float dt = gTimer.GetDeltaTime();
        
        gInput.Update();
        running = !gInput.Quit();
        Input::Move move = gInput.GetMove();
        if ( move != Input::NONE ) {
            gInput.Enable( false );
            int numTransitions = 0;
            switch ( move )
            {
            case Input::UP:
                numTransitions = gBoard.GetSlideUpTransitions( transitionBuf );
                break;
            case Input::DOWN:
                numTransitions = gBoard.GetSlideDownTransitions( transitionBuf );
                break;
            case Input::LEFT:
                numTransitions = gBoard.GetSlideLeftTransitions( transitionBuf );
                break;
            case Input::RIGHT:
                numTransitions = gBoard.GetSlideRightTransitions( transitionBuf );
                break;
            default:
                break;
            }
            for ( int tr = 0; tr < numTransitions; ++tr ) {
                transitionBuf[tr].curTime = 0.0f;
                transitionBuf[tr].endTime = 0.5f; // seconds
            }
            // animate the tiles moving
            bool doneTransition = false;
            while ( !doneTransition )
            {
                doneTransition = true;
                for ( int tr = 0; tr < numTransitions; ++tr ) {
                    transitionBuf[tr].curTime += dt;
                    doneTransition &= (transitionBuf[tr].curTime >= transitionBuf[tr].endTime);
                }

                gBoard.DrawWithTransitions( gRender, transitionBuf, numTransitions );

                unsigned int thisScore = gBoard.GetScore();
                if ( thisScore != lastScore ) {
                    sprintf( scoreStr, "SCORE: %u", thisScore );
                }
                lastScore = thisScore;
                int msgX = 25;
                int msgY = gFontBlack.getCharHeight();
                gFontBlack.Draw( gRender, msgX, msgY, scoreStr );

                msgX = 300;
                gFontBlack.Draw( gRender, msgX, msgY, highScoreStr );

                gInput.Update();
                gRender.Update();
                gTimer.Update();

                dt = gTimer.GetDeltaTime();
            }

            gInput.Enable( true );
            
            // actually move the tiles
            gBoard.ApplyLastTransitions();

            // we need to generate a new number piece
            int numFreeSpaces = gBoard.GetFreeSpaces( freeSpacesBuf );
            // no spaces left; game over
            if ( numFreeSpaces == 0 ) {
                running = false;
                break;
            }
            int newSpaceIndex = rand() % numFreeSpaces;
            Board::Space space = freeSpacesBuf[ newSpaceIndex ];
            // set the new location to 2
            gBoard.SetSpace( space.row, space.col, 2 );
        }

        gBoard.Draw( gRender );
        
        unsigned int thisScore = gBoard.GetScore();
        if ( thisScore != lastScore ) {
            sprintf( scoreStr, "SCORE: %u", thisScore );
        }
        lastScore = thisScore;
        int msgX = 25;
        int msgY = gFontBlack.getCharHeight();
        gFontBlack.Draw( gRender, msgX, msgY, scoreStr );

        msgX = 300;
        gFontBlack.Draw( gRender, msgX, msgY, highScoreStr );
        
        gRender.Update();
        gTimer.Update();
    }
}

static void GameOverScreen()
{
    char scoreStr[512];
    unsigned int finalScore = gBoard.GetScore();
    sprintf( scoreStr, "FINAL SCORE: %u", finalScore );
    const int finalScoreLen = strlen( scoreStr );
// PS2 build
#ifdef _EE
    static const char* playAgainStr = "PRESS CROSS TO PLAY AGAIN";
    static const int playAgainLen = strlen( playAgainStr );
// PC build
#else
    char playAgainStr[256];
    if ( SDL_NumJoysticks() > 0 ) {
        sprintf( playAgainStr, "PRESS B TO PLAY AGAIN" );
    } else {
        sprintf( playAgainStr, "PRESS SPACE TO PLAY AGAIN" );
    }
    const int playAgainLen = strlen( playAgainStr );
#endif
    bool running = true;
    while ( running )
    {
        float dt = gTimer.GetDeltaTime();
        (void)dt;
        
        gInput.Update();
        running = !gInput.Quit();
        running &= !gInput.Confirm();

        int msgX = (SCREEN_WIDTH/2) - (finalScoreLen*gFontBlack.getCharWidth())/2;
        int msgY = (SCREEN_HEIGHT/2) - gFontBlack.getCharHeight()*2;        
        gFontBlack.Draw( gRender, msgX, msgY, scoreStr );

        msgX = (SCREEN_WIDTH/2) - (playAgainLen*gFontBlack.getCharWidth())/2;
        msgY = (SCREEN_HEIGHT/2);
        gFontBlack.Draw( gRender, msgX, msgY, playAgainStr );

        gRender.Update();
        gTimer.Update();
    }
}

static void HighScoreScreen()
{
    char scoreStr[512];
    unsigned int finalScore = gBoard.GetScore();
    sprintf( scoreStr, "NEW HIGH SCORE: %u", finalScore );
    const int finalScoreLen = strlen( scoreStr );
    static const char* saveConfStr = "SAVE DATA?";
    static const char* yesStr = "YES";
    static const char* noStr = "NO";
    static const char* menuArrowStr = ">";
    
    int selection = 0;
    const int numSelections = 2;

    gSaveData.highScore = gBoard.GetScore();

    bool running = true;
    while ( running )
    {
        float dt = gTimer.GetDeltaTime();
        (void)dt;
        
        gInput.Update();
        running = !gInput.Quit();

        Input::Move playerMove = gInput.GetMove();
        if ( playerMove == Input::LEFT ) {
            --selection;
            if ( selection < 0 ) {
                selection = numSelections - 1;
            }
        }
        else if ( playerMove == Input::RIGHT ) {
            ++selection;
            if ( selection >= numSelections) {
                selection = 0;
            }
        }

        if ( gInput.Confirm() ) {
            // selection yes - save data to file
            if ( selection == 0 ) {
                if ( !gSaveData.Save() ) {
                    LOG_ERROR( "Failed to save data" );
                } else {
                    printf( "Successfully saved data\n" );
                }
            }
            running = false;
            continue;
        }

        int msgX = (SCREEN_WIDTH/2) - (finalScoreLen*gFontBlack.getCharWidth())/2;
        int msgY = 75 - gFontBlack.getCharHeight()*2;        
        gFontBlack.Draw( gRender, msgX, msgY, scoreStr );

        msgX = (SCREEN_WIDTH/2) - ( strlen(saveConfStr) * gFontBlack.getCharWidth() ) / 2;
        msgY += gFontBlack.getCharHeight()+10;
        gFontBlack.Draw( gRender, msgX, msgY, saveConfStr );

        msgX = (SCREEN_WIDTH/2) - 100;
        msgY += gFontBlack.getCharHeight() + 10;
        gFontBlack.Draw( gRender, msgX, msgY, yesStr );

        msgX += gFontBlack.getCharWidth()*strlen( yesStr ) + 100;
        gFontBlack.Draw( gRender, msgX, msgY, noStr );

        if ( selection == 0 ) {
            msgX = (SCREEN_WIDTH/2) - 125;
        } else {
            msgX -= 20;
        }
        gFontBlack.Draw( gRender, msgX, msgY, menuArrowStr );

        gRender.Update();
        gTimer.Update();
    }
}

static void TitleScreen()
{
    static const char* titleMsg = "2 0 4 8";
// PS2 build
#ifdef _EE
    static const char* beginMsg = "PRESS CROSS TO BEGIN";
// PC build
#else
    char beginMsg[256];
    if ( SDL_NumJoysticks() > 0 ) {
        sprintf( beginMsg, "PRESS B TO BEGIN" );
    } else {
        sprintf( beginMsg, "PRESS SPACE TO BEGIN" );
    }
#endif
    static const int titleLen = strlen( titleMsg );
    const int beginLen = strlen( beginMsg );
    bool running = true;
    while ( running )
    {
        gInput.Update();
        running = !gInput.Quit();
        if ( gInput.Confirm() ) {
            running = false;
        }

        int msgX = (SCREEN_WIDTH/2) - ((titleLen * gFontBlack.getCharWidth())/2);
        int msgY = (SCREEN_HEIGHT/2) - (gFontBlack.getCharHeight()/2);
        msgY -= gFontBlack.getCharHeight() * 2;
        gFontBlack.Draw( gRender, msgX, msgY, titleMsg );

        msgX = (SCREEN_WIDTH/2) - ((beginLen * gFont.getCharWidth())/2);
        msgY = (SCREEN_HEIGHT/2) - (gFontBlack.getCharHeight()/2);
        gFontBlack.Draw( gRender, msgX, msgY, beginMsg );

        gRender.Update();
        gTimer.Update();
    }
}

#ifdef _EE
static void testFile()
{
#if 0
    // for some reason putting this in PlatformInit() causes no sound to play
    printf( "Ps2 Platform Init Loading cdfs.irx buffer\n" );
    int irxRet = 0;
    int ret = SifExecModuleBuffer(
        cdfs_irx,
        size_cdfs_irx,
        0,
        nullptr,
        &irxRet
    );
    printf( "  cdfs ret, irx ret: %d, %d\n", ret, irxRet );

    char fileMsg[256];
    FILE* fp = fopen("cdfs:test.txt", "r");
    if ( !fp ) {
        printf( "Failed to open test.txt\n" );
        return;
    }
    fread( fileMsg, 1, 256, fp );
    fclose( fp );
    fp = nullptr;
    printf( "File data: %s\n", fileMsg );
#endif
}
#endif

int main(int argc, char* argv[])
{
    printf( "Platform Init\n" );
    EXIT_ON_FAIL( PlatformInit() )
    printf( "Music init\n" );
    EXIT_ON_FAIL( gMusic.Init( "stage1.wav" ) )
    printf( "Renderer init\n" );
    EXIT_ON_FAIL( gRender.Init() )
    printf( "Input init\n" );
    EXIT_ON_FAIL( gInput.Init() )
    printf( "Font init\n" );
    EXIT_ON_FAIL( gFont.Init( "font.png", 16, 16 ) )
    printf( "Fontblack init\n" );
    EXIT_ON_FAIL( gFontBlack.Init( "fontblack.png", 16, 16 ) )

#ifdef _EE
    testFile();
#endif

    if ( !gSaveData.Load() ) {
        gSaveData.highScore = 0;
    }
    
    gBoard.SetFont( gFont );
    gRender.SetBGColor( 255, 248, 229 ); // light cream
    
    srand( time(0) );

    EXIT_ON_FAIL( gMusic.Play( true ) )

    while ( true )
    {
        gBoard.Reset();
        gInput.Reset();
        TitleScreen();
        if ( gInput.Quit() ) {
            break;
        }
        GameplayLoop();
        if ( gInput.Quit() ) {
            break;
        }
        if ( gBoard.GetScore() > gSaveData.highScore ) {
            HighScoreScreen();
        }
        if ( gInput.Quit() ) {
            break;
        }
        GameOverScreen();
        if ( gInput.Quit() ) {
            break;
        }
    }
       
    return 0;
}

