#ifndef RENDERER_H_INCLUDED
#define RENDERER_H_INCLUDED

#include <SDL/SDL.h>

// forward declarations
class Image;

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

class Renderer
{
public:

    Renderer();
    ~Renderer();
    
    bool Init();

    void SetBGColor( unsigned char r, unsigned char g, unsigned char b ) {
        mBGColorR = r;
        mBGColorG = g;
        mBGColorB = b;
    }
    
    void DrawRect( int x, int y, int w, int h, 
        unsigned char r, unsigned char g, unsigned char b );

    void DrawImage( Image& img,
        int x, int y,
        int u = 0, int v = 0, int w = 0, int h = 0 );
    
    void Update();

private:
    SDL_Surface* mWindow;
    
    bool mLimitFps;
    Uint32 mLastTicks;

    unsigned char mBGColorR;
    unsigned char mBGColorG;
    unsigned char mBGColorB;
};

#endif // RENDERER_H_INCLUDED
