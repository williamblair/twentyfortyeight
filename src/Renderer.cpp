#include "Renderer.h"
#include "Image.h"
#include "Log.h"

Renderer::Renderer() :
    mWindow(nullptr),
    mLimitFps(true),
    mLastTicks(0),
    mBGColorR(0),
    mBGColorG(0),
    mBGColorB(0)
{
// PS2 build
#ifdef _EE
    mLimitFps = false;
#endif
}

Renderer::~Renderer()
{
    if (mWindow != nullptr) {
        SDL_FreeSurface(mWindow);
        mWindow = nullptr;
    }
}

bool Renderer::Init()
{
    mWindow = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_HWSURFACE );
    if ( !mWindow ) {
        LOG_ERROR( "Failed to set video mode" );
        return false;
    }

// PS2 build
#ifdef _EE
    SDL_ShowCursor( SDL_DISABLE );
#endif
    
    return true;
}

void Renderer::Update()
{
    SDL_Flip( mWindow );
    SDL_FillRect(
        mWindow,
        nullptr,
        SDL_MapRGB( mWindow->format, mBGColorR, mBGColorG, mBGColorB )
    );
    
    if ( mLimitFps ) {
        Uint32 thisTicks = SDL_GetTicks();
        Uint32 ticksPassed = thisTicks - mLastTicks;
        const Uint32 desiredFrameTicks = (Uint32)( 1000.0 / 60.0 );
        if ( ticksPassed < desiredFrameTicks ) {
            SDL_Delay( desiredFrameTicks - ticksPassed );
        }
        mLastTicks = thisTicks;
    }
}

void Renderer::DrawRect( int x, int y, int w, int h, 
        unsigned char r, unsigned char g, unsigned char b )
{
    SDL_Rect rect = { (Sint16)x, (Sint16)y, (Uint16)w, (Uint16)h };
    SDL_FillRect( mWindow, &rect, SDL_MapRGB( mWindow->format, r, g, b ) );
}

void Renderer::DrawImage( Image& img, int x, int y,
    int u, int v, int w, int h )
{
    SDL_Rect imgRect = { (Sint16)u, (Sint16)v, (Uint16)w, (Uint16)h };
    SDL_Rect posRect = { (Sint16)x, (Sint16)y, (Uint16)w, (Uint16)h };
    SDL_BlitSurface( img.mSurface, &imgRect, mWindow, &posRect );
}

