#ifndef PLATFORM_INIT_H_INCLUDED
#define PLATFORM_INIT_H_INCLUDED

// PS2 build
#ifdef _EE
    #include "PS2PlatformInit.h"
#endif

// PS3 build
#ifdef _PPU
    #include "PS3PlatformInit.h"
#endif

// PC Build
#ifdef _PC
    #include "PCPlatformInit.h"
#endif

#endif // PLATFORM_INIT_H_INCLUDED

