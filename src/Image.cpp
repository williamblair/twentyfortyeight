#include "Image.h"
#include "Log.h"

// PS2 build
#ifdef _EE
//#include <romfs_io.h>
#include <string.h>
// font.png array
#include "../data/font.h"
#include "../data/fontblack.h"
#else
#include <stdio.h>
#endif

bool Image::imgLibInitted = false;

Image::Image() :
    mWidth( 0 ),
    mHeight( 0 ),
    mSurface( nullptr )
{
}

Image::~Image()
{
    if ( mSurface != nullptr ) {
        SDL_FreeSurface( mSurface );
        mSurface = nullptr;
    }
}

#ifdef _EE
struct ImageArrayMap
{
    const char* fileName;
    unsigned char* fileData;
    unsigned int fileDataSize;
};
ImageArrayMap imgArrayMap[2] = {
    {
        "font.png",
        font_png, // in font.h
        size_font_png
    },
    {
        "fontblack.png", // in fontblack.h
        fontblack_png,
        size_fontblack_png
    }
};
const int imgArrayMapSize = 2;
#endif
bool Image::Init( const char* imageFile )
{
    if ( !imgLibInitted ) {
// PS2 build
#ifndef _EE
        int flags = IMG_INIT_JPG | IMG_INIT_PNG;
        int initted = IMG_Init( flags );
        if ( (initted & flags) != flags ) {
            LOG_ERROR( "Failed to init sdl image\n" );
            return false;
        }
#endif
        imgLibInitted = true;
    }

// PS2 build
#ifdef _EE
    SDL_RWops* imgRwops       = nullptr;

    // find the mapping array
    unsigned char* resultData = nullptr;
    unsigned int resultDataSize = 0;
    for ( int i = 0; i < imgArrayMapSize; ++i )
    {
        if ( strcmp( imgArrayMap[i].fileName, imageFile ) == 0 )
        {
            resultData = imgArrayMap[i].fileData;
            resultDataSize = imgArrayMap[i].fileDataSize;
            break;
        }
    }
    if ( resultData == nullptr ) {
        LOG_ERROR("Ps2 build failed to find file array");
        return false;
    }

    imgRwops = SDL_RWFromMem( resultData, resultDataSize );
    if ( imgRwops == nullptr ) {
        LOG_ERROR( "Failed to create imgRwops from fp" );
    }

    mSurface = IMG_Load_RW( imgRwops, 0 );
    if ( mSurface == nullptr ) {
        LOG_ERROR( "Failed to load image\n" );
        return false;
    }

    mWidth = mSurface->w;
    mHeight = mSurface->h;

    SDL_FreeRW( imgRwops ); imgRwops = nullptr;

    return true;
#endif

// PS3 build
#ifdef _PPU
    char*      imgFileMem     = nullptr;
    size_t     imgFileMemSize = 0;
    SDL_RWops* imgRwops       = nullptr;
    FILE*      fp             = nullptr;

    char fullPathName[256];
    sprintf( fullPathName, "%s/%s", ASSETS_DIR, imageFile );
    printf( "Img loading path name: %s\n", fullPathName );

    fp = fopen( fullPathName, "rb" );
    if ( fp == nullptr ) {
        LOG_ERROR( "Failed to open file" );
        return false;
    }
    fseek( fp, 0, SEEK_END );
    imgFileMemSize = ftell( fp );
    fseek( fp, 0, SEEK_SET );
    imgFileMem = new char[imgFileMemSize];
    if ( imgFileMem == nullptr ) {
        LOG_ERROR( "Failed to allocate image memory buffer" );
        fclose( fp );
        fp = nullptr;
        return false;
    }
    size_t amountRead = fread( (void*)imgFileMem, 1, imgFileMemSize, fp );
    if ( amountRead != imgFileMemSize ) {
        LOG_ERROR( "Failed to read expected image file amount" );
    }
    fclose( fp ); fp = nullptr;
    imgRwops = SDL_RWFromMem( imgFileMem, imgFileMemSize );
    if ( imgRwops == nullptr ) {
        LOG_ERROR( "Failed to create imgRwops from fp" );
    }

    mSurface = IMG_Load_RW( imgRwops, 0 );
    if ( mSurface == nullptr ) {
        LOG_ERROR( "Failed to load image\n" );
        return false;
    }

    mWidth = mSurface->w;
    mHeight = mSurface->h;

    delete[] imgFileMem; imgFileMem = nullptr;
    SDL_FreeRW( imgRwops ); imgRwops = nullptr;

    return true;
#endif

// PC build
#ifdef _PC
    char*      imgFileMem     = nullptr;
    size_t     imgFileMemSize = 0;
    SDL_RWops* imgRwops       = nullptr;
    FILE*      fp             = nullptr;

    char fullPathName[256];
    sprintf( fullPathName, "data/%s", imageFile );
    printf( "Img loading path name: %s\n", fullPathName );

    fp = fopen( fullPathName, "rb" );
    if ( fp == nullptr ) {
        LOG_ERROR( "Failed to open file" );
        return false;
    }
    fseek( fp, 0, SEEK_END );
    imgFileMemSize = ftell( fp );
    fseek( fp, 0, SEEK_SET );
    imgFileMem = new char[imgFileMemSize];
    if ( imgFileMem == nullptr ) {
        LOG_ERROR( "Failed to allocate image memory buffer" );
        fclose( fp );
        fp = nullptr;
        return false;
    }
    size_t amountRead = fread( (void*)imgFileMem, 1, imgFileMemSize, fp );
    if ( amountRead != imgFileMemSize ) {
        LOG_ERROR( "Failed to read expected image file amount" );
    }
    fclose( fp ); fp = nullptr;
    imgRwops = SDL_RWFromMem( imgFileMem, imgFileMemSize );
    if ( imgRwops == nullptr ) {
        LOG_ERROR( "Failed to create imgRwops from fp" );
    }

    mSurface = IMG_Load_RW( imgRwops, 0 );
    if ( mSurface == nullptr ) {
        LOG_ERROR( "Failed to load image\n" );
        return false;
    }

    mWidth = mSurface->w;
    mHeight = mSurface->h;

    delete[] imgFileMem; imgFileMem = nullptr;
    SDL_FreeRW( imgRwops ); imgRwops = nullptr;

    return true;
#endif
}

