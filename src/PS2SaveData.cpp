#include <cstdio>
#include "Log.h"
#include "SaveData.h"
#include "../data/ps2dev.h"

#include <tamtypes.h>
#include <kernel.h>
#include <sifrpc.h>
#include <loadfile.h>
#include <fcntl.h>
#include <unistd.h>
#include <sjis.h>
#include <malloc.h>
#include <libmc.h>
#include <stdio.h>
#include <string.h>

bool SaveData::Save()
{
    int fd;
    int ret;
    int mcType, mcFree, mcFormat;
    int mc_fd;
    int icon_fd;
    mcIcon icon_sys;
    bool saveExists = false;
    static iconIVECTOR bgcolor[4] = {
        { 68, 23, 116, 0 }, // top left
        { 255, 255, 255, 0 }, // top right
        { 255, 255, 255, 0 }, // bottom left
        { 68, 23, 116, 0 } // bottom right
    };
    static iconFVECTOR lightdir[3] = {
        { 0.5, 0.5, 0.5, 0.0 },
        { 0.0, -0.4, -0.1, 0.0 },
        { -0.5, -0.5, 0.5, 0.0 }
    };
    static iconFVECTOR lightcol[3] = {
        { 0.3, 0.3, 0.3, 0.00 },
        { 0.4, 0.4, 0.4, 0.00 },
        { 0.5, 0.5, 0.5, 0.00 }
    };
    static iconFVECTOR ambient = { 0.50, 0.50, 0.50, 0.00 };
    
    mcGetInfo(
        0, // port
        0, // slot
        &mcType, // int* type
        &mcFree, // int* free
        &mcFormat // int* format
    );
    // If this was the first call, -1 should be returned (switched to a formatted memory card)
    mcSync( 0, NULL, &ret );
    printf( "mcGetInfo returned %d\n", ret );
    printf( "  Type: %d Free: %d Format: %d\n", mcType, mcFree, mcFormat );

    if ( ret <= -2 ) {
        printf( "Unformatted or unrecognized memory card!\n" );
        return false;
    }

    // See if the save icon info already exists
    fd = open( "mc0:TW48/icon.sys", O_RDONLY );
    if ( fd <= 0 ) {
        printf( "No previous icon.sys exists, creating\n" );
        if ( mkdir( "mc0:TW48", 0777 ) < 0 ) {
            printf( "Failed to make save directory\n" );
            return false;
        }
    } else {
        printf( "Previous icon.sys exists, deleting then recreating\n" );
        close( fd );
        saveExists = true;
    }

    // setup icon.sys
    memset( &icon_sys, 0, sizeof(mcIcon) );
    strcpy( (char*)icon_sys.head, "PS2D" ); // header == PS2D
    strcpy_sjis( (short*)&icon_sys.title, "2048 Game\nBJ Blair" );
    icon_sys.nlOffset = 10; // new line offset in title
    icon_sys.trans = 0x60; // transparency
    memcpy( icon_sys.bgCol, bgcolor, sizeof(bgcolor) );
    memcpy( icon_sys.lightDir, lightdir, sizeof(lightdir) );
    memcpy( icon_sys.lightCol, lightcol, sizeof(lightcol) );
    memcpy( icon_sys.lightAmbient, ambient, sizeof(ambient) );
    strcpy( (char*)icon_sys.view, "ps2dev.icn" ); // these filenames are relative to the directory in which icon.sys resides
    strcpy( (char*)icon_sys.copy, "ps2dev.icn" );
    strcpy( (char*)icon_sys.del, "ps2dev.icn" );

    // testing putting the save data in the unknown at the end
    memcpy( (char*)icon_sys.unknown3, &highScore, sizeof(highScore) );

    // write icon.sys
    int flags = O_WRONLY;
    if ( !saveExists ) {
        flags |= O_CREAT;
    }
    mc_fd = open( "mc0:TW48/icon.sys", flags);
    if ( mc_fd < 0 ) {
        printf( "Failed to open icon.sys fd\n" );
        return false;
    }
    write( mc_fd, &icon_sys, sizeof(icon_sys) );
    close( mc_fd );
    printf( "Write icon.sys\n" );

    // write ps2dev.icn
    icon_fd = open( "mc0:TW48/ps2dev.icn", flags );
    if ( icon_fd < 0 ) {
        printf( "Failed to open ps2dev.icn\n" );
        return false;
    }
    write( icon_fd, ps2dev_icn, size_ps2dev_icn ); // included in ../data/ps2dev.h
    close( icon_fd );
    printf( "ps2dev.icn written successfully\n" );

    // write the actual save data
    /*fd = open( "mc0:TW48/savedata.bin", O_WRONLY | O_CREAT );
    if ( fd < 0 ) {
        printf( "Failed to open savedata.bin\n" );
        return false;
    }
    write( fd, (char*)&highScore, sizeof(highScore) );
    close( fd );
    printf( "savedata.bin written successfully\n" );*/
    
    return true;
}

bool SaveData::Load()
{
    int fd;
    int ret;
    int mcType, mcFree, mcFormat;
    mcIcon icon_sys;
    
    mcGetInfo(
        0, // port
        0, // slot
        &mcType, // int* type
        &mcFree, // int* free
        &mcFormat // int* format
    );
    // If this was the first call, -1 should be returned (switched to a formatted memory card)
    mcSync( 0, NULL, &ret );
    printf( "mcGetInfo returned %d\n", ret );
    printf( "  Type: %d Free: %d Format: %d\n", mcType, mcFree, mcFormat );

    if ( ret <= -2 ) {
        printf( "Unformatted or unrecognized memory card!\n" );
        return false;
    }

    // see if save data already exists
    fd = open( "mc0:TW48/icon.sys", O_RDONLY );
    if ( fd <= 0 ) {
        printf( "No savedata.bin exists in load\n" );
        return false;
    }
    read( fd, (void*)&icon_sys, sizeof(icon_sys) );
    printf( "Read icon sys\n" );
    close( fd );

    // test extract high score from icon sys
    memcpy( (void*)&highScore, (void*)icon_sys.unknown3, sizeof(highScore) );

    return true;
}

