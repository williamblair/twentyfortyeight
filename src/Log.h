#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED

//#include <string>

#define LOG_ERROR(msg) \
    Log::Error( __FILE__, __LINE__, msg )

namespace Log
{

void Error( const char* file, const int line, const char* message );

} // end namespace Log

#endif // LOG_H_INCLUDED
