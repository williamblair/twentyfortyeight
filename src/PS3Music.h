#include <SDL/SDL.h>
#include <SDL/SDL_endian.h>
#include <SDL/SDL_mixer.h>

class Music
{
public:

    Music();
    ~Music();

    bool Init( const char* fileName );

    bool Play( const bool looping );

private:
    Mix_Chunk* mMusic;
};


