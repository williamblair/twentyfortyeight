BIN  = main
OBJS = build/main.o \
       build/Renderer.o \
       build/Log.o \
       build/Input.o \
       build/GameTimer.o \
       build/Font.o \
       build/Image.o \
       build/Board.o \
       build/Music.o \
       build/PCSaveData.o

CC     = g++
CFLAGS = -g -Wall -std=c++11 `pkg-config --cflags sdl` -D_PC=1
INCDIRS = 
LIBDIRS = 
LIBS = -lSDL -lSDL_image -lSDL_mixer

build/%.o: src/%.cpp
	$(CC) $(CFLAGS) -c -o $@ $< $(INCDIRS)

all: $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(BIN) $(LIBDIRS) $(LIBS)

clean:
	rm -rf $(OBJS) $(BIN)
